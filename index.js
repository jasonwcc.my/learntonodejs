var os = require("os");
var hostname = os.hostname();


const http = require('http');
const port = process.env.PORT || 8080;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  const msg = 'Hello World!, Welcome to J web server\nRuntime : Node.js\nHost : '+hostname
  res.end(msg);
});

server.listen(port, () => {
  console.log(`Server running on http://localhost:${port}/`);
});